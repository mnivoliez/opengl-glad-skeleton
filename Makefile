CXXFLAGS=-Wall -std=c++17 -Wextra -ldl -lGL -lglfw -g -Iglad/include/

app: main.cpp glad/glad.c
	$(CXX) $(CXXFLAGS) main.cpp glad/glad.c -o app
